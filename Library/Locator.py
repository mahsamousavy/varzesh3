
class Locator:
    baseUrl = "https://www.varzesh3.com/"
    scrollTo = "window.scrollTo(0,document.body.scrollHeight)"
    tableForeignLig = '#widget21 > div.widget-toolbar > div > select'
    laligaPath = '#widget21 > div.widget-toolbar > div > select > option:nth-child(5)'
    rowTablePath = '//*[@id="widget21"]/div[4]/div/div/div/div/table/tbody/tr[{}]'
    viewTable = '#widget21 > div.widget-content.md-padding > div > div > div > a'
    localHostUrl = 'http://localhost:4723/wd/hub'
    getItemLocalStorage = "return window.localStorage.getItem(arguments[0]);"
    setItemLocalStorage = "window.localStorage.setItem(arguments[0], arguments[1]);"
    tablePath = '//*[@id="widget21"]/div[4]/div/div/div/div/table/tbody'
    browserName = "Chrome"
    platformName = "android"
    deviceName = "Pixel 3 XL"
    automationName = "UiAutomator2"
    true = "true"
    udidMobilePhone ="8A5Y0FEF5"
    chromeDriverPath = "/Users/Mahsaa/desktop/chromedriver/chromedriver"
    platformVersion = '11'
    rankNumber = 'rank'
    teamName = 'teamName'
    barselonaTeamName = 'بارسلونا'
    tatenhamTeamName = 'تاتنهام'
    errorMessageBarselonaIsNotExist = f"'بارسلونا'is not exist"
    errorMessageTatenhamIsexist = f"'تاتنهام'is exist"


