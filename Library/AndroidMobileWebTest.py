import time
from urllib.parse import urljoin, quote
from robot.libraries.BuiltIn import BuiltIn
from selenium import webdriver
from appium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait as wDw
from selenium.webdriver.common.by import By
from Library.Locator import Locator


class AndroidMobileWebTest:

    def __init__(self):
        self.baseUrl = Locator.baseUrl
        self.log = BuiltIn()

    def open_browser_in_android(self):

        desired_capabilities = {
            "browserName": Locator.browserName,
            "platformName": Locator.platformName,
            "deviceName": Locator.deviceName,
            "headspin:autoDownloadChromedriver": Locator.true,
            "automationName": Locator.automationName,
            "autoAcceptAlerts": Locator.true,
            "udid": Locator.udidMobilePhone,
            "chromedriverExecutable": Locator.chromeDriverPath,
            'platformVersion': Locator.platformVersion,
            'autoGrantPermissions': Locator.true,
            "chromeOptions": {"args": ['--incognito', '--disable-notifications']}
        }

        self.driver = webdriver.Remote(Locator.localHostUrl, desired_capabilities)

#   open varzesh3
    def open_url(self):
        self.driver.get(self.baseUrl)

#   scroll to down
    def scroll_to(self):
        self.driver.execute_script(Locator.scrollTo)

#   click to جدول لیگ های خارجی
    def select_laliga_in_table(self):
        wDw(self.driver, 10).until(ec.visibility_of_element_located((By.CSS_SELECTOR, Locator.tableForeignLig)))
        self.driver.find_element_by_css_selector(Locator.tableForeignLig).click()
        wDw(self.driver, 10).until(ec.visibility_of_element_located((By.CSS_SELECTOR, Locator.laligaPath)))
#   select laliga in table
        self.driver.find_element_by_css_selector(Locator.laligaPath).click()

#   select table data
    def get_laliga_table_data(self):
        wDw(self.driver, 10).until(ec.visibility_of_element_located((By.XPATH, Locator.tablePath)))
        textTable = self.driver.find_element_by_xpath(Locator.tablePath).text
        return textTable

#   get table dataa and stor in list
    def save_laliga_table_data(self):
        list = []
        time.sleep(5)
        for index in range(1, 21):
            singleData= {}
            rowPathText = Locator.rowTablePath.format(index)
            rankNumber = self.driver.find_element_by_xpath(rowPathText + f"/td[1]").text
            teamName = self.driver.find_element_by_xpath(rowPathText + f"/td[2]").text
            singleData.update({Locator.rankNumber: rankNumber})
            singleData.update({Locator.teamName: teamName})
            list.append(singleData)
        return list

#  click on مشاهده جدول کامل لالیگا
    def click_on_view_full_laliga_table(self):
        wDw(self.driver, 10).until(ec.visibility_of_element_located(
            (By.CSS_SELECTOR, Locator.viewTable)))
        self.driver.find_element_by_css_selector(Locator.viewTable).click()
        time.sleep(5)

# assert بارسلونا in table
    def barselona_team_name_assert(self):
        assert Locator.barselonaTeamName in AndroidMobileWebTest.get_laliga_table_data(self), Locator.errorMessageBarselonaIsNotExist

# find rank of barselona in table
    def find_rank_of_barselona_in_table(self):
        for item in range(len(AndroidMobileWebTest.save_laliga_table_data(self))):
            for key, value in AndroidMobileWebTest.save_laliga_table_data(self)[item].items():
                if value == Locator.barselonaTeamName:
                    self.log.log_to_console(AndroidMobileWebTest.save_laliga_table_data(self)[item].get(Locator.rankNumber))
                    break
            break

# assert تاتنهام in table
    def tatenham_team_name_assert(self):
        assert Locator.tatenhamTeamName not in AndroidMobileWebTest.get_laliga_table_data(self), Locator.errorMessageTatenhamIsexist

# get from localstorage
    def get_local_storage(self, key):
        return self.driver.execute_script(Locator.getItemLocalStorage, key)

# set to localstorage
    def set_local_storage(self, key, value):
        self.driver.execute_script(Locator.setItemLocalStorage, key, value)

# Store second team name in browser localStorage as teamName and generate the url of the team page
    def generate_url_team_name(self, teamNameItem):
        teamName= quote(AndroidMobileWebTest.save_laliga_table_data(self)[teamNameItem-1].get(Locator.teamName).encode("UTF-8"))
        AndroidMobileWebTest.set_local_storage(self, teamName, teamName)
        urlSecondTeamName = urljoin(self.baseUrl, f"team/{AndroidMobileWebTest.get_local_storage(self, teamName)}")
        return urlSecondTeamName

# open the first team page by go to url
    def go_to_url_first_page(self, teamNameItem):
#   swich chrome tab
        self.driver.switch_to.window(self.driver.window_handles[0])
#   open the first team page by go to url
        self.driver.get(AndroidMobileWebTest.generate_url_team_name(self, teamNameItem))

    def tear_down(self):
        self.driver.quit()






