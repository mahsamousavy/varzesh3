
# varzesh3
The purpose of this project is to automate varzesh3's web page testing in Android mobile process accordinr to testcase.

# Getting Started
Clone the project:
git clone https://gitlab.com/mahsamousavy/varzesh3

# Installation
Download (https://github.com/appium/appium-desktop/releases/) and setup Appium (see http://appium.io/docs/en/about-appium/getting-started/)
To be able to run these tests you need to have Python installed on your computer. You need also to install Selenium and Appium-Python-Client:

With pip
pip install Appium-Python-Client

With brew
brew install appium

Download Chromedrivert from https://chromedriver.chromium.org/downloads According to the Chrome version of the phone and copy in your computer and set in project

# setup in appium
{
  "browserName": "Chrome",
  "platformName": "android",
  "deviceName": "Pixel 3 XL",
  "automationName": "UiAutomator2",
  "autoAcceptAlerts": "true",
  "udid": "8A5Y0FEF5",
  "chromedriverExecutable": "./chromedriver/chromedriver"
}

# Install python libraries

pip3 install -r requirements.txt
pip3 install -U selenium
pip3 install Appium-Python-Client
python3 -m pip install robotframework



Running the tests
You can run tests by run.py
For example :
python3 -m run.py

Rules of Thumb
In this section there will be a short document on how to code according to the project's standards.

General project rules:

Naming rules:

No upper case letters
No spaces
No symbols
No dashes.

Always use lower case letters and underlines for naming functions, classes and variables

I used mobile device pixle 3xl with android version 11.



