*** Settings ***
Library    Library/AndroidMobileWebTest.py
Test Setup  Set Log Level  TRACE
*** Test Cases ***
Test chrome
    open browser in android
    open url
    scroll to
    select laliga in table
    get laliga table data
    save laliga table data
    click on view full laliga table
    barselona team name assert
    find rank of barselona in table
    tatenham team name assert
    set local storage  key  value
    get local storage  key
    generate url team name  teamNameItem=${2}
    generate url team name  teamNameItem=${1}
    go to url first page  teamNameItem=${1}
    tear down
